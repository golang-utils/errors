package errors_test

import (
	"errors"
	"fmt"
	"testing"

	. "gitlab.com/golang-utils/errors"
)

func TestError(t *testing.T) {
	var (
		ErrCantOpenFile     = Error("can't open file %q: %w")
		ErrFileDoesNotExist = fmt.Errorf("file does not exist")
	)

	err := ErrCantOpenFile.Params("missing.exe", ErrFileDoesNotExist)

	if !errors.Is(err, ErrCantOpenFile) {
		t.Errorf("err should be ErrCantOpenFile, but is not")
	}

	if !errors.Is(err, ErrFileDoesNotExist) {
		t.Errorf("err should be ErrFileDoesNotExist, but is not")
	}

	errInner := errors.Unwrap(err)

	if errInner != ErrFileDoesNotExist {
		t.Errorf("the wrapped error should be ErrFileDoesNotExist, but is not")
	}

	got := err.Error()
	expected := `can't open file "missing.exe": file does not exist`

	if got != expected {
		t.Errorf("err.Error() == %q // expected %q", got, expected)
	}
}

func TestErrorWithCreatorFunc(t *testing.T) {
	var (
		ErrCantOpenFile     = Error("can't open file %q: %w")
		ErrFileDoesNotExist = fmt.Errorf("file does not exist")
	)

	var NewErrCantOpenFile = func(file string, innerError error) error {
		return ErrCantOpenFile.Params(file, innerError)
	}

	err := NewErrCantOpenFile("missing.exe", ErrFileDoesNotExist)

	if !errors.Is(err, ErrCantOpenFile) {
		t.Errorf("err should be ErrCantOpenFile, but is not")
	}

	if !errors.Is(err, ErrFileDoesNotExist) {
		t.Errorf("err should be ErrFileDoesNotExist, but is not")
	}

	errInner := errors.Unwrap(err)

	if errInner != ErrFileDoesNotExist {
		t.Errorf("the wrapped error should be ErrFileDoesNotExist, but is not")
	}

	got := err.Error()
	expected := `can't open file "missing.exe": file does not exist`

	if got != expected {
		t.Errorf("err.Error() == %q // expected %q", got, expected)
	}
}

func TestErrorWrappedHidden(t *testing.T) {
	var (
		ErrCantOpenFile     = Error("can't open file %q")
		ErrFileDoesNotExist = fmt.Errorf("file does not exist")
	)

	err := ErrCantOpenFile.Wrap(ErrFileDoesNotExist).Params("missing.exe")

	if !errors.Is(err, ErrCantOpenFile) {
		t.Errorf("err should be ErrCantOpenFile, but is not")
	}

	if !errors.Is(err, ErrFileDoesNotExist) {
		t.Errorf("err should be ErrFileDoesNotExist, but is not")
	}

	errInner := errors.Unwrap(err)

	if errInner != ErrFileDoesNotExist {
		t.Errorf("the wrapped error should be ErrFileDoesNotExist, but is not")
	}

	got := err.Error()
	expected := `can't open file "missing.exe"`

	if got != expected {
		t.Errorf("err.Error() == %q // expected %q", got, expected)
	}
}

func TestErrorWrappedHiddenNoParams(t *testing.T) {
	var (
		ErrCantOpenFile     = Error("can't open file")
		ErrFileDoesNotExist = fmt.Errorf("file does not exist")
	)

	err := ErrCantOpenFile.Wrap(ErrFileDoesNotExist)

	if !errors.Is(err, ErrCantOpenFile) {
		t.Errorf("err should be ErrCantOpenFile, but is not")
	}

	if !errors.Is(err, ErrFileDoesNotExist) {
		t.Errorf("err should be ErrFileDoesNotExist, but is not")
	}

	errInner := errors.Unwrap(err)

	if errInner != ErrFileDoesNotExist {
		t.Errorf("the wrapped error should be ErrFileDoesNotExist, but is not")
	}

	got := err.Error()
	expected := `can't open file`

	if got != expected {
		t.Errorf("err.Error() == %q // expected %q", got, expected)
	}
}

func TestErrors(t *testing.T) {
	var (
		ErrCantOpenFile     = Error("can't open file %q")
		ErrFileDoesNotExist = fmt.Errorf("file does not exist")
		ErrWeatherNotFine   = fmt.Errorf("the weather is bad")
	)

	err := Errors(
		ErrCantOpenFile.Wrap(ErrFileDoesNotExist).Params("missing.exe"),
		ErrWeatherNotFine,
	)

	if !errors.Is(err, ErrCantOpenFile) {
		t.Errorf("err should be ErrCantOpenFile, but is not")
	}

	if !errors.Is(err, ErrFileDoesNotExist) {
		t.Errorf("err should be ErrFileDoesNotExist, but is not")
	}

	if !errors.Is(err, ErrWeatherNotFine) {
		t.Errorf("err should be ErrWeatherNotFine, but is not")
	}

	err1 := errors.Unwrap(err)

	if err1 == nil {
		t.Errorf("err1 is nil")
		return
	}

	if !errors.Is(err1, ErrCantOpenFile) {
		t.Errorf("err1 should be ErrCantOpenFile, but is %s", err1.Error())
	}

	err2 := errors.Unwrap(err)

	if err2 == nil {
		t.Errorf("err2 is nil")
		return
	}

	if err2 != ErrWeatherNotFine {
		t.Errorf("err2 should be ErrWeatherNotFine, but is %s", err2.Error())
	}

	got := err.Error()
	expected := `[0] can't open file "missing.exe"
[1] the weather is bad
`

	if got != expected {
		t.Errorf("err.Error() got\n%q \n// expected \n%q", got, expected)
	}

}

func TestMap(t *testing.T) {
	var (
		ErrCantOpenFile     = Error("can't open file %q")
		ErrFileDoesNotExist = fmt.Errorf("file does not exist")
		ErrWeatherNotFine   = fmt.Errorf("the weather is bad")
	)

	var err = ErrMap{
		"file": "missing.exe",
		"errors": Errors(
			ErrCantOpenFile.Wrap(ErrFileDoesNotExist).Params("missing.exe"),
			ErrWeatherNotFine,
		),
	}

	if !errors.Is(err, ErrCantOpenFile) {
		t.Errorf("err should be ErrCantOpenFile, but is not")
	}

	if !errors.Is(err, ErrFileDoesNotExist) {
		t.Errorf("err should be ErrFileDoesNotExist, but is not")
	}

	if !errors.Is(err, ErrWeatherNotFine) {
		t.Errorf("err should be ErrWeatherNotFine, but is not")
	}

	if !errors.Is(err, err) {
		t.Errorf("err should be err, but is not")
	}

	if err.Get("file") != "missing.exe" {
		t.Errorf("file is not correct")
	}

	errs := errors.Unwrap(err)

	err1 := errors.Unwrap(errs)

	if err1 == nil {
		t.Errorf("err1 is nil")
		return
	}

	if !errors.Is(err1, ErrCantOpenFile) {
		t.Errorf("err1 should be ErrCantOpenFile, but is %s", err1.Error())
	}

	err2 := errors.Unwrap(errs)

	if err2 == nil {
		t.Errorf("err2 is nil")
		return
	}

	if err2 != ErrWeatherNotFine {
		t.Errorf("err2 should be ErrWeatherNotFine, but is %s", err2.Error())
	}

	got := err.Error()
	expected := `errors: [0] can't open file "missing.exe" [1] the weather is bad 
file: missing.exe
`

	if got != expected {
		t.Errorf("err.Error() got\n%q \n// expected \n%q", got, expected)
	}
}
