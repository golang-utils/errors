package errors

import (
	"errors"
	"fmt"
	"reflect"
	"sort"
	"strings"
)

type errWithArgs struct {
	es         Error
	wrappedErr error
	args       []interface{}
}

func (e *errWithArgs) Is(target error) bool {
	res := errors.Is(e.es, target)
	return res
}

func (e *errWithArgs) Unwrap() error {
	return e.wrappedErr
}

func (e *errWithArgs) Error() string {
	return fmt.Errorf(string(e.es), e.args...).Error()
}

func (e *errWithArgs) Params(args ...interface{}) error {
	e.args = args
	return e
}

func (e Error) Wrap(err error) *errWithArgs {
	return &errWithArgs{
		es:         e,
		wrappedErr: err,
	}
}

type Error string

func (e Error) Error() string {
	return string(e)
}

func (e Error) Params(args ...interface{}) error {
	eArgs := &errWithArgs{
		es:   e,
		args: args,
	}
	for _, x := range args {
		if err, isErr := x.(error); isErr {
			eArgs.wrappedErr = err
		}
	}
	return eArgs
}

type ErrorCollection struct {
	idx    int
	Errors []error
}

func (e *ErrorCollection) Error() string {
	var bd strings.Builder

	for i, err := range e.Errors {
		bd.WriteString(fmt.Sprintf("[%v] %s\n", i, err.Error()))
	}

	return bd.String()
}

func (e *ErrorCollection) Is(target error) bool {
	for _, err := range e.Errors {
		if errors.Is(err, target) {
			return true
		}
	}

	return false
}

func (e *ErrorCollection) Unwrap() error {
	if e.idx < len(e.Errors) {
		err := e.Errors[e.idx]
		if e.idx < len(e.Errors)-1 {
			e.idx += 1
		}
		return err
	}
	return nil
}

func Errors(errs ...error) *ErrorCollection {
	return &ErrorCollection{
		idx:    0,
		Errors: errs,
	}
}

type ErrMap map[string]interface{}

func (e ErrMap) Get(key string) interface{} {
	return e[key]
}

func (e ErrMap) Error() string {
	var bd strings.Builder

	var keys []string

	for k := range e {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	for _, k := range keys {
		v := e[k]
		val := v
		if err, isErr := v.(error); isErr {
			val = strings.ReplaceAll(err.Error(), "\n", " ")
		}
		bd.WriteString(fmt.Sprintf("%s: %v\n", k, val))
	}

	return bd.String()
}

func (e ErrMap) Unwrap() error {
	for _, v := range e {
		if err, is := v.(error); is {
			return err
		}
	}
	return nil
}

func (e ErrMap) Is(target error) bool {
	if reflect.DeepEqual(e, target) {
		return true
	}
	for _, v := range e {
		if err, is := v.(error); is {
			return errors.Is(err, target)
		}
	}
	return false
}
