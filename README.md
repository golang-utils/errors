# errors

library for easily helping with errors.

## License

MIT

## Documentation

https://pkg.go.dev/gitlab.com/golang-utils/errors

## Installation

```
go get gitlab.com/golang-utils/errors@latest
```

## Usage

(everything can be combined freely)

### simple 

```go

import (
	"errors"
	. "gitlab.com/golang-utils/errors"
)


var (
	ErrCantOpenFile     = Error("can't open file %q: %w")
	ErrFileDoesNotExist = errors.New("file does not exist")
)

// also without params call usable
err := ErrCantOpenFile.Params("missing.exe", ErrFileDoesNotExist)

// is true
errors.Is(err, ErrCantOpenFile) 

// is true
errors.Is(err, ErrFileDoesNotExist)

// errInner is ErrFileDoesNotExist
errInner := errors.Unwrap(err)

// returns `can't open file "missing.exe": file does not exist`
err.Error()

```

### hidden wrapped error

```go

import (
	"errors"
	. "gitlab.com/golang-utils/errors"
)

var (
	ErrCantOpenFile     = Error("can't open file %q")
	ErrFileDoesNotExist = errors.New("file does not exist")
)

// also without params call usable
err := ErrCantOpenFile.Wrap(ErrFileDoesNotExist).Params("missing.exe")

// is true
errors.Is(err, ErrCantOpenFile) 

// is true
errors.Is(err, ErrFileDoesNotExist)

// is ErrFileDoesNotExist
errInner := errors.Unwrap(err)

// returns `can't open file "missing.exe"`
err.Error()

```

### Collection of errors

```go

import (
	"errors"
	. "gitlab.com/golang-utils/errors"
)


var (
	ErrCantOpenFile     = Error("can't open file %q")
	ErrFileDoesNotExist = errors.New("file does not exist")
	ErrWeatherNotFine   = errors.New("the weather is bad")
)

err := Errors(
	ErrCantOpenFile.Wrap(ErrFileDoesNotExist).Params("missing.exe"),
	ErrWeatherNotFine,
)

// all of this is true
errors.Is(err, ErrCantOpenFile)
errors.Is(err, ErrFileDoesNotExist)
errors.Is(err, ErrWeatherNotFine)

// unwraps the first error
err1 := errors.Unwrap(err)

// is true
err1 == ErrCantOpenFile)

// unwraps the second error
err2 := errors.Unwrap(err)

// is true
err2 == ErrWeatherNotFine

// is true: there is no third error (ErrFileDoesNotExist can be unwrapped from err1)
errors.Unwrap(err) == nil
```

### Map 

```go

import (
	"errors"
	. "gitlab.com/golang-utils/errors"
)

var (
	ErrCantOpenFile     = Error("can't open file %q")
	ErrFileDoesNotExist = errors.New("file does not exist")
	ErrWeatherNotFine   = errors.New("the weather is bad")
)

var err = ErrMap{
	"file": "missing.exe",
	"errors": Errors(
		ErrCantOpenFile.Wrap(ErrFileDoesNotExist).Params("missing.exe"),
		ErrWeatherNotFine,
	),
}

// all of this is true
errors.Is(err, ErrCantOpenFile)
errors.Is(err, ErrFileDoesNotExist)
errors.Is(err, ErrWeatherNotFine)

// is true
err.Get("file") == "missing.exe" 

// get the error collection back
errs := errors.Unwrap(err)

// first entry in the error collection
err1 := errors.Unwrap(errs)

// is true
errors.Is(err1, ErrCantOpenFile) 
```